# Arjhay Frias

*I am a Software Developer with 1 month Experience on Flutter Development as a Trainee and Developing Game since 2018.*

*My scope ranges from web, mobile and game development, Really love studying Algorithms and Game Development Techniques.*

# Contact Information

*LinkedIn:  [*linkedin.com/in/arjhay-frias/*](http://linkedin.com/in/arjhay-frias/)*

*Website Portfolio: [*https://frias-about-me-app.herokuapp.com/*](https://frias-about-me-app.herokuapp.com/)*

## **Work Experience**

# **Flutter Developer Trainee**

**FFUF, July 21 to Present**

- *I was trained to not just Dart and Flutter alone but OOP, DevOps, best Coding Practices and numorous Tools to speed up the development process.*

# **Game Developer**

**Freelance 2018 to Present**

- *I Created 2D Platformer game projects for my Clients.*

## **Skills**

# **Technical Skills**

- *Programming*
- *Software Engineering*
- *Game Development*

# **Soft Skills**

- *Team Player*
- *Problem-Solver*
- *Open-Mindedness*
- *Resilient*
- *Creativity*

# Education

## **BS Computer Science**

**AMA CC Fairview, 2020-present**